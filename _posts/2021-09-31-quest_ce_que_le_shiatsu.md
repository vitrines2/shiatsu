---
layout: post
title:  Qu’est-ce que le shiatsu ?
date:   2021-09-31
tags:   [Shiatsu]
---


Le shiatsu est une technique manuelle et énergétique, originaire du Japon, qui utilise des pressions et des étirements sur les méridiens d’acupuncture. C’est donc une approche orientale de la santé, holistique et préventive. 

En équilibrant la circulation de l’énergie, le shiatsu permet un mieux-être corporel et mental, il favorise le bon fonctionnement des organes et stimule l’immunité.

Le shiatsu permet la détente, le lâcher prise, il régule la fonction digestive, respiratoire, circulatoire, soulage les douleurs musculaires, articulaires, les maux de tête…Il agit sur les capacités de récupération en cas de fatigue, surmenage, décalage horaire, il équilibre le sommeil…

Le shiatsu s’adresse à tous les âges de la vie. Il peut être bénéfique dans l’accompagnement de certaines pathologies en complément du suivi médical habituel. **Dans tous les cas, les prescriptions et recommandations du médecin sont indispensables et doivent être strictement respectées.**

Le shiatsu se pratique au sol sur un futon. Il est important d’être habillé confortablement avec des vêtements amples, chauds suivant la saison. Chaque séance est unique car elle répond au rééquilibrage énergétique du jour mais peut aussi s’inscrire dans un objectif à plus long terme. Elle vise à stimuler vos capacités d’auto-guérison et vous engage de façon active à l’amélioration de votre santé.

