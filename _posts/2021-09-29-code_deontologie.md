---
layout: post
title:  Code de déontologie du SPS
date:   2021-09-29
tags:   [Shiatsu]
---

Tout membre actif du Syndicat Professionnel de Shiatsu s’engage, dès son inscription, au respect du code de déontologie conforme à l’esprit du Shiatsu, à la lettre des Statuts, au Règlement Intérieur du SPS, aux lois et règlements en vigueur, tant français qu’européens.

En conséquence, il s’engage également sur l’honneur à :

* exercer son art dans le respect total de l’intégrité physique et morale de la personne,
* respecter une stricte confidentialité,
* toujours garantir une prestation optimale, notamment en maintenant ses compétences au plus haut niveau à l’aide de cours, stages et formations complémentaires,
* mener ses activités de Shiatsu en excluant toute forme de prosélytisme confessionnel, politique ou sectaire ; ce qui constituerait un motif de radiation.

En outre, il doit garder à l’esprit que le Shiatsu n’est ni une pratique médicale au sens occidental du terme, ni un massage, ni une idéologie mais **un art s’inscrivant prioritairement dans le domaine de la prévention et, plus généralement, du bien-être.**

Par conséquent, il doit :
* s’abstenir d’établir un quelconque diagnostic,
* se garder d’interrompre ou de modifier un traitement médical,
* s’interdire de prescrire ou conseiller des médicaments,
* diriger sans délai vers un médecin toute personne se plaignant ou présentant les signes d’un malaise.
