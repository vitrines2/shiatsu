---
layout: page
title: Qui suis-je ?
permalink: /about/
---


![Marie-Paule BEL]({{site.baseurl}}/images/portrait.jpg)

# Marie-Paule BEL

<img style="text-align: center;" src="{{site.baseurl}}/images/label.png" alt="Spécialiste en Shiatsu - Praticienne certifiée" width="200"/> 

**Titre professionnel RNCP**
**Syndicat des Professionnels de Shiatsu**
RIDET : 1 115 740


***********

Mon intérêt pour les pratiques corporelles remonte au choix de mes études universitaires en psychomotricité. Passer par le corps pour soigner le psychisme et l’émotionnel a été mon premier travail. J’ai pratiqué à cette époque l’Eutonie de Gerda Alexander qui vise un rééquilibrage du tonus musculaire par la relaxation, les étirements et les massages à l’aide d’objets (balles ou baguettes). 

Depuis mon arrivée en Nouvelle-Calédonie, il y a plus de trente ans, les médiations corporelles et la psychomotricité ont été au centre de mon travail d’institutrice tout d’abord en maternelle puis après des enfants en grande difficulté scolaire.   

Dans les lieux où j’ai vécu à travers le pays, différentes rencontres m’ont amenée à découvrir la méditation, le Qi Gong, le Yoga et depuis 2013 le Shiatsu et le Do In. Dans le même temps, j’ai privilégié au sein de ma famille, une approche de la santé préventive et holistique basée sur l’homéopathie, l’acuponcture, la naturopathie et les plantes médicinales kanak.

Dans une démarche de développement personnel, j’ai participé à des ateliers d’écoute active, basée sur l’approche centrée sur la personne de Carl Rogers. Cette expérience de groupe m’a permis de développer en tant qu’écoutée et écoutante, l’empathie, la congruence, le regard positif inconditionnel et le non jugement. C’est une approche analogue que j’ai retrouvée dans le shiatsu. 

Le Shiatsu auquel j’ai été formée à Nouméa est le Zen Shiatsu, un courant développé par Shizuto Masunaga. Il s’agit d’un shiatsu des méridiens basé sur l’énergétique traditionnelle chinoise. C’est une prise en compte du fonctionnement holistique de la personne avec les interactions étroites qui unissent les aspects physiques, émotionnels, psychiques et spirituels. 

Mon expérience en shiatsu m’a amenée à travailler avec des publics divers tels que les personnes accueillies à la Maison Maternelle Marcelle Jorda, les jeunes de la maison Gabriel Poédi et à ceux de l’Institut Spécialisé Autisme, des personnes atteintes d’AVC, des femmes enceintes… Il m’importe de faire découvrir les bienfaits du shiatsu au plus grand nombre. Après avoir partagé un cabinet à Dumbéa, j’ai le plaisir d’ouvrir un nouvel espace dédié au Shiatsu à Mont-Dore Sud.
