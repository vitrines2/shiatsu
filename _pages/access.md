---
layout: page
title: Accès
permalink: /access/
---

## Pour me contacter : 

**téléphone** : 78.53.44

**mail** : belshiatsu [arobase] gmail.com

Je reçois **sur rendez-vous**.

Horaires : 

- à domicile : les mardi de 13h à 18h
- au cabinet : les lundi, jeudi et vendredi de 10h à 18h

## Où suis-je ?


Mon cabinet se trouve dans l'impasse du départ du sentier du Mont-Dore au 386 Impasse des inséparables, Mont-Dore Sud (anciennement 27 Rue du colibri)

![Plan]({{site.baseurl}}/images/plan.jpg)

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9559.205335095688!2d166.59340676270958!3d-22.287229713604894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6c280f911fcdbffb%3A0x6eeb381d752fe3b4!2sShiatsu%20et%20Do-in!5e1!3m2!1sfr!2sfr!4v1631868057959!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
